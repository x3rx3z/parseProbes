A basic tool for parsing through probe requests.

Filtering by beacon count aids in ignoring the current environment, since 
static local devices will clock up many beacons over time. Similarly cars
driving through the filtered radius typically only give off one beacon or so.

Filtering by distance helps to ignore transient clients on the edge of the antenna
range.


usage;

	./sniffProbes wlan0mon | ./parseProbes.py


Tested on Kali x86/x64/ARM (Beaglebone Black)


Get sniffProbes @ https://gitlab.com/x3rx3z/sniffProbes

![alt text](img/img.png)