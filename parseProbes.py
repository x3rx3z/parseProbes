#!/usr/bin/python -i

import sys, time, os, collections
from flask import Flask, render_template, request, url_for, redirect
from threading import Thread, Lock
from multiprocessing import Process

app = Flask(__name__)

# Dict of observed clients and their details
clients = collections.OrderedDict()
# Range filter 
filterMinDistance = 0.0
filterMaxDistance = 1000.0
# Beacon Filter
filterMinBeacons = 0.0
filterMaxBeacons = 1000000
# Duration filter
filterMinDuration = 0.0
filterMaxDuration = 1000000
# First seen filter
filterMinFirstSeen = 0
filterMaxFirstSeen = 32503680000 # yr 3000 
# Client filter
filterClients = set()


keysByBeacon = []
keysByDistance = []

# POST
#    minDistance 
#    maxDistance
@app.route('/')
def client_list():

    return render_template('clients.html', 
            client_list = clients, 
            filter_list = filterClients, 
            min_dist = filterMinDistance, 
            max_dist = filterMaxDistance,
            min_beacons = filterMinBeacons,
            max_beacons = filterMaxBeacons,
            min_duration = filterMinDuration,
            max_duration = filterMaxDuration,
            min_seen = filterMinFirstSeen,
            max_seen = filterMaxFirstSeen)



@app.route('/filterDistances', methods=['GET', 'POST'])
def filterDistances():
    global filterMinDistance
    global filterMaxDistance
    global filterMinBeacons
    global filterMaxBeacons
    global filterMinFirstSeen
    global filterMaxFirstSeen
    global filterMinDuration
    global filterMaxDuration

    inputMinDist = request.form.get('inputMinDist', '')
    inputMaxDist = request.form.get('inputMaxDist','')

    if inputMinDist:
        try:
            filterMinDistance = float(inputMinDist)
        except ValueError:
            pass
        
    if inputMaxDist:
        try:
            filterMaxDistance = float(inputMaxDist)
        except ValueError:
            pass

    inputMinBeacons = request.form.get('inputMinBeacons', '')
    inputMaxBeacons = request.form.get('inputMaxBeacons', '')

    if inputMinBeacons:
        try:
            filterMinBeacons = int(inputMinBeacons)
        except ValueError:
            pass
        
    if inputMaxBeacons:
        try:
            filterMaxBeacons = int(inputMaxBeacons)
        except ValueError:
            pass

    inputMinFirstSeen = request.form.get('inputMinSeen', '')
    inputMaxFirstSeen = request.form.get('inputMaxSeen', '')
    pattern = "%m/%d/%Y %I:%M %p"
    
    if inputMinFirstSeen:
        inputMinFirstSeen  = time.mktime(time.strptime( inputMinFirstSeen, pattern ))
        filterMinFirstSeen = inputMinFirstSeen
    
    if inputMaxFirstSeen:
        inputMaxFirstSeen  = time.mktime(time.strptime( inputMaxFirstSeen, pattern ))
        filterMaxFirstSeen = inputMaxFirstSeen
 

    inputMinDuration = request.form.get('inputMinDuration','')
    inputMaxDuration = request.form.get('inputMaxDuration','')

    if inputMinDuration:
        try:
            filterMinDuration = float(inputMinDuration)
        except ValueError:
            pass

    if inputMaxDuration:
        try:
            filterMaxDuration = float(inputMaxDuration)
        except ValueError:
            pass

    return redirect(url_for('client_list'))



@app.route('/filterClient/<filterMac>')
def filterClient( filterMac ):
    global filterClients
    if filterMac == "clear_list":
        filterClients = set()
    else:
        filterClients.add( filterMac )
    return redirect(url_for('client_list'))


@app.route('/vectors/<clientMac>')
def vectors( clientMac ):
    try:
        retStr = ''
        for vector in clients[ clientMac ]['vectors']:
            retStr += str(vector[0])+','+str(vector[1])+','+str(vector[2])+"<br>"
        return retStr
    except KeyError:
        return redirect(url_for( 'client_list' ))


def reader( infile ):
    while True:
        # Read a line from stdin (clone)
        line  = infile.readline()
        elts  = line.strip().split('|')
        
        if len(elts) == 4:
           
            clientMAC  = elts[0]
            clientSSID = elts[1]
            clientRSSI = elts[2]
            clientDist = elts[3]

           # Stamp with current time
            stamp = time.asctime()#.split()[-2]
            epoch = time.time()
            try:
                if clients[ clientMAC ]:
                    print "adding to existing"
                    firstEpoch = clients[ clientMAC ]['firstEpoch']
                    clients[ clientMAC ]['ssids'].add( clientSSID )
                    clients[ clientMAC ]['lastSeen']  = stamp
                    clients[ clientMAC ]['lastEpoch'] = epoch
                    clients[ clientMAC ]['duration'] = round(epoch - firstEpoch, 2)
                    clients[ clientMAC ]['beacons']  += 1
                    clients[ clientMAC ]['vectors'].append( (stamp, epoch,\
                                            float(clientDist[:-1])) )
                    print stamp,'- - ', clientMAC, ' : ', \
                            str([ x for x in clients[ clientMAC ]['ssids'] ]), \
                            ' @ ', clientDist

            except KeyError:
                print "creating new"
                clients[ clientMAC ] = {
                    # Set of assoicated SSIDS
                    'ssids': set(), 
                    'firstSeen': stamp,
                    'firstEpoch': epoch,
                    'lastSeen': stamp,
                    'lastEpoch': epoch,
                    'duration': 0.0,
                    'beacons': 1,
                    # List of tuple (time, dist) vectors
                    'vectors': [(stamp, epoch, float(clientDist[:-1]))]
                    }
                clients[ clientMAC ]['ssids'].add( clientSSID )
                print stamp, ' - - ', clientMAC, ' : ', clientSSID, ' @ ', \
                        clientDist
            
            keysByBeacon = sorted( clients.items(), \
                    key=lambda x: x[1]['beacons'], reverse=True)       
            
            keysByDistance = sorted( clients.items(), \
                    key=lambda x: x[1]['vectors'][-1][1], reverse=True)
if __name__ == "__main__":
    dupin = os.fdopen(os.dup(sys.stdin.fileno()))
    p = Thread(target=reader, args=(dupin,))
    p.daemon = True
    p.start()
    
    app.run(host="0.0.0.0", debug=True, use_reloader=False)


